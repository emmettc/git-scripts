#!/bin/bash
  if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
    echo "$(basename $0) - Show stash changes"
    exit
  fi
  
function usage
  {
  echo 'Usage: gsh [ options ] [ stash number ]'
  echo
  echo 'Options:'
  echo '  -h or ? prints this usage information.'
  echo
  echo '  -d Show diff of changed files'
  echo
  exit 1
  }

diff=
while getopts "dh" opt; do
  case $opt in
    d)
      diff="-p"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

  idx=1
  list=`git stash list | awk '{$1=$2=""; print $0}'`
  
if [ -z "$list" ]; then
  echo 'Nothing stashed'
  exit
fi

while read -r st; do
  sts[$idx]="$st"
  echo "${idx}. $st"
  let idx=$[idx + 1]
done <<< "$list"
  
if [ -z "$1" ]; then
  if [ -n "$stidx" ]; then
    idx="$stidx"
  else
    echo
    echo -n "Select stash to show (CR to exit): "
    read idx
    if [ -z "$idx" ]; then
      exit
    fi
  fi  
else
  idx=$1
fi  

stash=${sts[$idx]}
  
if [ -z "$stash" ]; then
  echo "Invalid selection ($idx)"
  exit
fi

let idx=($idx - 1)
#echo "git stash show $diff $idx"
git stash show $diff $idx
