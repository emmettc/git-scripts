#!/bin/bash
if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
  echo "$(basename $0) - Stage all modified files, commit and push to branch"
  exit
fi


function usage
  {
  echo 'Usage: gap [ OPTIONS ] [ branch_name ]'
  echo
  echo 'Stages all modified and possibly untracked files, commits branch then pushes it to origin.'
  echo 'Push is enabled by default.'
  echo
  echo 'Options:'
  echo '  -h Print this usage information.'
  echo
  echo '  -m Commit message. You must bracket message with quotes.'
  echo
  echo "  -y Don't prompt, just stage, commit and possibly push."
  echo
  echo "  -P Don't push to origin."
  echo
  echo "  -u Don't add untracked files."
  echo
  echo "  -B Don't build production assets.  Only required for Laravel repos."
  echo
  echo "  -o Alternate origin name.  Default is 'origin', of course."
  echo
  echo "  -c Translate provided branch name to uppercase"
  echo
  echo '  -n Dry run (do nothing).  Enables verbose.'
  echo
  echo '  -v Verbose'
  echo
  echo ' If you do not provide a message (-m option), you will be prompted within the EDITOR.'
  echo
  echo ' If you do not provide a branch name the current branch will be staged, committed and possibly pushed.'
  echo
  exit 1
  }

untracked='true'
origin='origin'
build_assets='true'
to_push_msg=', commit and push'
while getopts "CBcUuym:o:vnPph" opt; do
  case $opt in
    C|c)
      do_tr='true'
      ;;
    u|U)
      untracked=
      ;;
    y)
      no_prompt='true'
      ;;
    B)
      build_assets=
      ;;
    m)
      msg="$OPTARG"
      ;;
    o)
      origin="$OPTARG"
      origin_msg=" to '$origin'"
      ;;
    v)
      verbose='true'
      ;;
    n)
      dry='true'
      verbose='true'
      ;;
    P|p)
      no_push='true'
      to_push_msg=' and commit'
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

dir=`pwd`
asset_root=
if [[ $dir == *"work"* ]]; then
  asset_root='Laravel/'
fi

if [ $# -eq 0 ];then
  br=`git branch --show-current`
else
  br="$1"
  if [ -n $do_tr ]; then
    br=`echo $br | tr '[:lower:]' '[:upper:]'`
  fi
fi  
  
if [ -n "$build_assets" ]; then
  echo 'Building production assets'
  if [ -e "${asset_root}vite.config.js" ]; then
    np build
    bld='true'
  elif [ -e "${asset_root}webpack.mix.js" ]; then
    wp
    bld='true'
  fi
  if [ -n "$bld" ]; then
    echo -n "Build a success? Continue stage$to_push_msg '$br'$origin_msg? - (Ycn) "
    read -n 1 ans
    if [ "$ans" = 'n' ] || [ "$ans" = 'N' ];then
      echo
      exit
    fi  
    if [ "$ans" = 'c' ] || [ "$ans" = 'C' ];then
      skip_prompt='true'
    fi
  fi
  echo
fi
if [ -z "$no_prompt" ] && [ -z "$skip_prompt" ];then
  echo -n "Are you sure you want to stage$to_push_msg '$br'$origin_msg? - (yN) "
  read -n 1 ans
  if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
    echo
    exit
  fi  
    echo
else    
  echo -n "Staging, committing and pushing '$br'$origin_msg."
fi

if [ -z "$untracked" ];then
  if [ -n "$verbose" ]; then
    echo "git add -u"
  fi  
  if [ -z "$dry" ]; then
    git add -u
  fi  
else
  if [ -n "$verbose" ]; then
    echo "git add -A"
  fi  
  if [ -z "$dry" ]; then
    git add -A
  fi  
fi

if [ -n "$msg" ];then
  if [ -n "$verbose" ]; then
    echo "git commit -m\"'$msg'\""
  fi
  if [ -z "$dry" ]; then
     git commit -m"'$msg'"
  fi  
else
  if [ -n "$verbose" ]; then
    echo "git commit"
  fi
  if [ -z "$dry" ]; then
    git commit
  fi  
fi

if [ -z "$no_push" ]; then
  if [ -n "$verbose" ]; then
    echo "git push $origin $br"
  fi  
  if [ -z "$dry" ]; then
    git push "$origin" "$br"
  fi
elif [ -n "$verbose" ]; then
  echo "No push to $origin $br"
fi
