#!/bin/bash
  if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
    echo "$(basename $0) - List stashes to drop."
    exit
  fi
  
function is_numeric ()
  {
  re='^[0-9]+$'
  if ! [[ $1 =~ $re ]] ; then
    return 0
  fi
  return 1
  }

function usage
  {
  echo 'Usage: gsd [ options ] [ stash number ]'
  echo
  echo " Drops stash(es) without applying to workspace."
  echo
  echo 'Options:'
  echo '  -h or ? prints this usage information.'
  echo
  echo '  -s stash #'
  echo
  echo "  -y Don't prompt, just drop it"
  echo
  echo '  -n Dry run (do nothing).  Enables verbose.'
  echo
  echo '  -v be verbose'
  echo
  exit 1
  }

while getopts "s:hyvn" opt; do
  case $opt in
    s)
      stidx="$OPTARG/"
      ;;
    y)
      no_prompt='true'
      ;;
    n)
      dry_run='true'
      verbose='true'
      ;;
    v)
      verbose='true'
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

idx=0
list=`git stash list | awk '{$1=$2=""; print $0}'`
  
if [ -z "$list" ]; then
  echo 'Nothing stashed'
  exit
fi


while read -r st; do
  let idx=$[idx + 1]
  sts[$idx]="$st"
  echo "${idx}. $st"
done <<< "$list"


if [ -n "$1" ]; then 
  is_numeric $1
  if [ $? -eq 1 ]; then
    stash=${sts[$1]}
    let idx=$1
  fi
fi

let idx_len=1
if [ -z "$stash" ]; then
  echo
  echo -n "Enter index of stash(es) to drop (CR to exit): "
  read line
  if [ -z "$line" ]; then
    exit
  fi
  idxs=($line)
  let idx_len=${#idxs[@]}
  if [ $idx_len -eq 1 ]; then
    let idx=${idxs[0]}
    stash=${sts[$idx]}
    if [ -z "$stash" ]; then
      echo "Invalid selection"
      exit
    fi
  else
    # sort indexes decending as we need to drop in reverse order.  This is because the stash index
    # re orders when any element other than the last one is removed.
    IFS=$'\n'
    sorted=($(sort -r <<<"${idxs[*]}"))  
    for i in "${sorted[@]}"; do
      st="${sts[$i]}"
      if [ -n "$st" ]; then
        stashes[$i]="$st"
      fi  
    done
  fi
fi  

if [ $idx_len -eq 1 ]; then
  if [ -z "$stash" ]; then
    echo 'You must supply a stash index on command line or via the -s option.'
    usage
  fi

  if [ -z "$no_prompt" ];then
    echo -n "Are you sure you want to drop '$stash' - (yN) "
    read -n 1 ans
    if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
      echo
      exit
    fi  
      echo
  fi
  if [ -n "$verbose" ]; then
    echo "git stash drop $stash ($idx)"
  fi  
  if [ -z "$dry_run" ]; then
    let idx=($idx - 1)
    git stash drop $idx
  fi  
else
  unset IFS
  if [ -z "$no_prompt" ];then
    echo "Are you sure you want to drop multiple stashes?"

    for st in "${stashes[@]}"; do
       if [ -n "$st" ]; then
        echo -e "\t$st"
       fi
    done
    echo -n "(yN) "
    read -n 1 ans
    if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
      echo
      exit
    fi  
  fi
  echo
  if [ -z "$dry_run" ]; then
    echo "Dropped:"
  else
    echo "Would have dropped:"
  fi  

  for idx in "${sorted[@]}"; do
      let idx=($idx - 1)
      let ii=($idx + 1)
      if [ -z "$dry_run" ]; then
        if [ -n "$verbose" ]; then
          echo "cmd = git stash drop $idx"
        fi  
        git stash drop "$idx" >/dev/null 
        echo -e "\t${stashes[$ii]}"
      else
        echo -e "\t${stashes[$ii]}, $ii"
      fi  
  done
fi
echo
exit
