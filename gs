#!/bin/bash
  if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
    echo "$(basename $0) - Status short cut (short form)"
    exit
  fi

function usage()
  { 
  echo 'gs [ options ] [ search_string ]'
  echo
  echo 'Display list of modified and new files (untracked)'
  echo
  echo 'Opiton:'
  echo
  echo ' -h Display this help '
  echo 
  echo ' -c Only display count of changed files.'
  echo 
  echo ' -p Include files in public directory.'
  echo 
  echo ' -x Display long form.'
  echo
  echo 'If search_string is included, then the list is filtered to show'
  echo 'only those files that match the search string'
  echo
  exit;
  }

  while getopts "hcxp" opt; do
  case $opt in
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    x)
      full='true' 
      ;;
    c)
      count='true' 
      ;;
    p)
      public='true' 
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
#  echo "opt = $opt"
done
shift $((OPTIND-1))

if [ $# -gt 0 ]; then
    search="$1"
fi
  
# git status
cmd="/usr/bin/git -c color.status=always status"
if [ -z "$full" ]; then 
  cmd="$cmd -s"
elif [ -n "$search" ]; then
  echo "No search for normal output (when using -x option)"
  echo
  search=
fi
if [ -n "$search" ]; then
  cmd="$cmd | grep $search"
fi  
if [ -z "$public" ]; then
  cmd="$cmd | grep -v public"
fi
if [ -n "$count" ]; then
  echo -n 'Changed file count = '
  cmd="$cmd | wc -l"
fi
eval $cmd
#echo "cmd = $cmd"
echo
br=`git symbolic-ref --short HEAD`
echo "Current branch = $br"
