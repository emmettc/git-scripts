#!/bin/bash

if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
  echo "$(basename $0) - List all branches you can merge"
  exit
fi

function is_numeric ()
  {
  re='^[0-9]+$'
  if ! [[ $1 =~ $re ]] ; then
    return 0
  fi
  return 1
}

function usage
  {
  echo 'Usage: gml [ options ] [ branch # | name ]'
  echo
  echo 'Options:'
  echo '  -h prints this usage information.'
  echo
  echo "  -y Don't prompt, just merge it"
  echo
  echo '  -b Branch name to merge.  You can also name a branch or its index as a non-option parameter.'
  echo
  exit 1
  }

while getopts "b:hy" opt; do
  case $opt in
    b)
      branch="$OPTARG"
      ;;
    y)
      no_prompt='true'
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

if [ -n "$1" ]; then 
  is_numeric $1
  if [ $? -eq 0 ]; then
    branch_provided='true'
  fi
fi

idx=0
if [ -z "$branch_provided" ]; then  
  list=`git branch`
  while read -r br; do
    # git puts asterisk at the beginning of the branch  that is currently checked out.
    if [[ ${br:0:1} != '*' ]]; then
      let idx=$[idx + 1]
      brs[$idx]=$br
      desc=`git config branch.$br.description`
      echo "${idx}. $br $desc"
    else
      current=${br:2}
    fi  
  done <<< "$list"
fi

if [ -n "$1" ]; then 
  if [ -z "$branch_provided" ]; then
    branch=${brs[$1]}
  else
    branch="$1"
  fi
fi

if [ -n "$current" ]; then
  echo "Current branch = $current"
  echo
fi
  
if [ -z "$branch" ]; then
  echo
  echo -n "Select branch to merge with '$current' (CR to exit): "
  read idx
  if [ -z "$idx" ]; then
    exit
  fi
  branch=${brs[$idx]}
  if [ -z "$branch" ]; then
    echo "Invalid selection"
    exit
  fi
fi

if [ -z "$branch" ]; then
  echo 'You must supply a branch name or number.  Use non option parameter or the -b option.'
  usage
fi

if [ -z "$no_prompt" ];then
  echo -n "Are you sure you want to merge '$branch' into '$current' - (yN) "
  read -n 1 ans
  if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
    echo
    exit
  fi  
    echo
fi

git merge $branch
echo
exit
