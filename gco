#!/bin/bash
if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
  echo "$(basename $0) - List modified files to checkout from origin (reset)"
  exit
fi

function usage
  {
  echo "Usage: $(basename $0) [ options ] [ item_index | search_string]"
  echo
  echo 'Options:'
  echo '  -h prints this usage information.'
  echo
  echo '  -f File name (path) to checkout'
  echo
  echo '  -d Include deleted and copied files'
  echo
  echo '  -u Include unmerged files'
  echo
  echo '  -p Include files in "public" or "fontello" directories.'
  echo
  echo '  -a Search AND. default is OR when more that one string separated by | is prorvided for search'
  echo
  echo '  -D Sort by changed date.'
  echo
  echo '  -i Include items that match the search text. Otherwise show only those'
  echo '     items that do not match the search string. Ignored if -p option included.'
  echo
  echo "  -y Don't prompt, just checkout file(s)"
  echo
  echo "At the select prompt you can list multiple indexes for those files you want to reset.  Separate each with a space."
  echo
  exit 1
  }

file=
origin='origin'  
while getopts "hydpuif:" opt; do
  case $opt in
    f)
      file="$OPTARG"
      ;;
    y)
      no_prompt='true'
      ;;
    d)
      inc_deleted='-d'
      ;;
    a)
      and='-a'
      ;;
    u)
      inc_unmerged='-u'
      ;;
    p)
      search_type='-tpub'
      ;;
    D)
      by_date='-D '
      ;;
    i)
      if [ "$search_type" != '-tpub' ]; then
        search_type='-tinc'
      fi  
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

. list_functions.sh

if [ -n "$1" ]; then 
  is_numeric $1
  if [ $? -eq 0 ]; then
    search="-s$1"
  fi
fi

if [ -z "$file" ]; then  
  list=`git status --porcelain`
#echo "ordered_list $search $search_type $inc_deleted $inc_unmerged $by_date"
  ordered_list $search $and $search_type $inc_deleted $inc_unmerged $by_date
  if [ $? -eq 0 ]; then
    echo 'No modified or new file to reset.'
    echo
    exit
  fi
  
  if [ -n "$1" ]; then 
    is_numeric $1
    if [ $? -eq 0 ]; then
      file=${lines[$1]}
    fi
  fi
fi

let idx_len=1
if [ -z "$file" ]; then
  echo
  echo -n "Enter index file(s) or '*' for all, to add (stage) - (CR to exit): "
  read line
  if [ -z "$line" ]; then
    exit
  fi
  if [ "$line" = '*' ];then
    let idx_len=0
    for fl in "${lines[@]}"; do
      if [ -n "$fl" ]; then
        if [ $idx_len -eq 0 ];then
          file=${lines[${idxs[0]}]}
        fi
        let "idx_len++"
        files+=("$fl")
      fi  
    done <<< $lines
  else
  idxs=($line)
    let idx_len=${#idxs[@]}
    if [ $idx_len -eq 1 ]; then
      file=${lines[${idxs[0]}]}
      if [ -z "$file" ]; then
        echo "Invalid selection"
        exit
      fi
    else
        for i in "${idxs[@]}"; do
        fl="${lines[$i]}"
        if [ -n "$fl" ]; then
          files+=("$fl")
        fi  
      done
    fi
  fi
fi

if [ $idx_len -eq 1 ]; then
  if [ -z "$file" ]; then
    echo 'You must supply an index or file/path via the -f option.'
    usage
  fi

  if [ -z "$no_prompt" ];then
    echo "Are you sure you want to reset"
    echo -e "\t$file"
    echo -n "(yN) "
    read -n 1 ans
    if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
      echo
      exit
    fi  
      echo
  fi
  git checkout $file
else
  if [ -z "$no_prompt" ];then
    echo "Are you sure you want to reset"
    for fl in "${files[@]}"; do
       echo -e "\t$fl"
    done
    echo -n "(yN) "
    read -n 1 ans
    if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
      echo
      exit
    fi  
  fi  
  echo
  echo "Checked out (reset):"
  for fl in "${files[@]}"; do
    git checkout $fl >/dev/null 2>&1
    echo -e "\t$fl"
  done
fi
echo
exit
