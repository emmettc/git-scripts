Clone into some directory, then add cloned directory to your path.

Example:
``` console
# cd /home/myhome/bin

# git clone git@gitlab.com:emmettc/git-scripts.git 
```
Add this to your personal **.bashrc** file or the system **/etc/bashrc** file:
``` bash
if ! [[ "$PATH" =~ "$HOME/bin/git-scripts" ]]; then
  export PATH="$HOME/bin/git-scripts:$PATH"
fi
```
Don't forget to reload bash to include your new path.
``` console
# bash
```
Or start a new terminal instance.

Use the command **glist** to list available commands with minimal info on each command.
```console
# glist
```
If you follow glist with a string then only commands with that string or partial string in the minimal info will be listed.

Example:

```console
# glist stash
gsa - List stashes to apply
gsd - List stashes to drop.
gsh - Show stash changes
gsl - List stashes to pop
gss - Stash using provided stash name

Use -h option after any command to see usage instructions
```
As indicated in the output from the command above, you can see that all commands will provide more detailed usage information when the **-h** option is provided.

Example:
```console
# gcl -h
Usage: gcl [ options ] [ branch # | name ]

Prints numbered list of currently defined branches and allows selection of 
branch to checkout.

Options:
  -h prints this usage information.

  -y Don't prompt, just check it out

  -c Create new branch. You must provide a new branch name, which will be 
     appended to 'feature/' by default.

  -b Branch name to create or checkout, without conversion to upper case.
     You can also name a branch or its index as a non-option parameter.

  -n Don't prefix branch name provided with 'feature/'

  -r Set prefix to 'release/', overriding default.
     This also tags the branch with the value given for branch name. 
     Be sure to use x.y.z with numeric values for x, y and z for the branch name.

  -x Set prefix to 'hotfix/', overriding default.

  -p Branch name prefix, overriding default.  eg. -p hotfix will cause the branch name
     to be prefixed with 'hotfix/'.  Default is 'feature/'.

  -C do not translate to uppercase

 Note that branch names provided on the command line, except when using the -b option,
 are converted to uppercase by default.
```
All commands that will affect your work space will prompt to continue.  However, most commands do have an option to not prompt.

Some commands that allow you to select from a list allow you to enter multiple items to operate on.
