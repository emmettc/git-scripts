#!/bin/bash
if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
  echo "$(basename $0) - Contains only function to be called by other scripts in this directory"
  exit
fi
stat=
#inc=()

function list_usage
  {
  echo 'Usage: list_functions.sh'
  echo
  echo 'is_numeric'
  echo '  Returns 1 if first parameter is a number'
  echo
  echo 'ordered_list'
  echo '  Order by path unless -D option is provided.'
  echo '  Exclude paths with public or fontello unless -p option is provided.'
  echo '  Include only files that match -s option arg in the path, unless -t option = excl, then exclude'
  echo '  those files that match.'
  echo
  echo 'ordered_list options:'
  echo '  -h or ? prints this usage information.'
  echo
  echo '  -s exclude or include these files.'
  echo
  echo '  -t Search type'
  echo '     excl = exclude -s argument value. This is the default'
  echo '     inc = include -s argument value.'
  echo "     pub = include files in 'public' or 'fontello' directories.  Default is to exclude those files"
  echo
  echo '  -S List only staged files.  The default is to list unstaged modified files.'
  echo
  echo '  -u Include unmerged files.  The default is to not include unmerged files.'
  echo
  echo '  -r Include renamed files.  The default is to not include renamed files.'
  echo
  echo '  -C Retain search string capitalization'
  echo
  echo '  -T Include untracked files.  The default is to not include untracked files.'
  echo
  echo '  -d Include deleted or copied files.  The default is to not list those files.'
  echo
  echo '  -D Sort by date instead of path.'
  echo
  echo 'Search for multiple strings by separating each with "|".'
  exit 1
  }

function contains ()
{
#echo "filter = ${inc[@]}" >&2
   local include=("$@")
#echo "looking for '$stat'"   >&2
    # always ignore untracked files
    if [ -z $stat ]; then
      return 0
    fi
  for itm in "${include[@]}"; do
    if [ "$itm" = "$stat" ]; then
#echo "found $stat"   >&2
     return 1
    fi  
  done 
#echo "stat = |$stat|"    
  return 0
}

function get_line ()
{
#echo "inc = ${inc[*]}"
  local incc
  local OPTIND=1
  while getopts "d" opt; do
    case $opt in
      d)
        inc_date='true'
        ;;
      \?)
        echo "Invalid option: -$OPTARG" >&2
        ;;
      :)
        echo "Option -$OPTARG requires an argument." >&2
        ;;
    esac
  done
  shift $((OPTIND-1))
  
#  local line="$1"
  local NL=$'\n'
  IFS=' '
#echo "f line = '$line'" 
  #Read the split words into an array based on space delimiter
  read -a item <<< "$line"
#echo "item = '$item', item[0] = '${item[0]}', item[1] = '${item[1]}', item[2] = '${item[2]}'"
  # treat renamed and copied files differently
  local stat=${line:0:3};
#echo "f stat = '$stat'"  
  incc=('R  ' ' R ' 'C  ' ' C ')
  contains "${incc[@]}"
#echo "ret = $ret"  
  if [ $? -eq 1 ]; then
    file="${item[3]}"
  else
    file="${item[1]}"
  fi
#echo "file = $file"  
  if [ -n "$inc_date" ]; then
    # prepend file update date and time
    if [ -e $file ]; then
      # file exists so get date and time stats
      stat=$(stat -c%y $file); 
      date=`echo $stat | awk '{ print $1 }'`
      timex=`echo $stat | awk '{ print $2 }'`
      # split off micro seconds
      time=`echo $timex | awk -F '.' '{ print $1 }'`
      line_out="$NL$date $time $file"
    else
      # file doesn't exist so no stats to get, and we still want it listed.
      line_out="$NL deleted - $file"
    fi
  else
    line_out="$NL$file"
  fi
#echo "line_out = '$line_out'" >&2  
}

# Get list of files sorted by path or date
# We can get unstaged or staged files. see -S option
function ordered_list
  {
  local OPTIND=1
  local search_type='inc'
  local search=
  local staged=
  local by_date=
  while getopts "TCqrhuas:t:SdUD" opt; do
#echo "opt = $opt, arg = '$OPTARG'"  >&2
    case $opt in
      a)
        and='true'
        ;;
      s)
        search="$OPTARG"
        ;;
      t)
        search_type="$OPTARG"
        ;;
      S)
        staged='true'
        ;;
      u)
        unmerged='true'
        ;;
      C)
        retain_caps='true'
        ;;
      U)
        unmerged='true'
        only_unmerged='true'
        ;;
      T)
        inc_untracked='true'
        ;;
      D)
        by_date='-d'
        ;;
      d)
        inc_deleted='true'
        ;;
      q)
        quiet='true'
        ;;
      h)
        list_usage
        ;;
      \?)
        echo "Invalid option: -$OPTARG" >&2
        list_usage
        ;;
      :)
        echo "Option -$OPTARG requires an argument." >&2
        list_usage
        ;;
    esac
  done
  shift $((OPTIND-1))

  local file
  if [ -z "$list" ]; then
    return 0
  fi
  out=
  NL=$'\n'
  IFS=$'\n' #' '
  # see git status manual for what the contents of $inc means
  if [ -z "$only_unmerged" ]; then
    if [ -n "$staged" ]; then
      inc=('M  ' 'T  ' 'A  ' 'MM ')
      if [ -n "$inc_deleted" ]; then 
        inc+=('D  ' 'C  ')
      fi  
      if [ -n "$inc_renamed" ]; then 
        inc+=('R  ')
      fi  
    else
      inc=( ' M ' ' T ' )
      if [ -n "$inc_deleted" ]; then 
        inc+=( ' A ' ' D ' 'D  ' ' C ' )
      else
        inc+=(' R ')
      fi
      if [ -n "$inc_untracked" ]; then 
        inc+=( '?? ' )
      fi
    fi
  fi  
  if [ -n "$unmerged" ]; then
    inc+=( 'U  ' ' U ' 'UU ' )
    if [ -n "$inc_deleted" ]; then 
      inc+=( 'UD ' 'DU ' )
    fi
  fi
#echo "inc = ${inc[@]}" >&2
#printf '|%s|\n' "${inc[@]}"      
#echo
#  let idx=0
if [ -n "$search" ]; then
#echo "search = '$search', search type = '$search_type', by date = '$by_date'" >&2
  if [ -z "$retain_caps" ]; then
#    search=`echo $search | tr '[:upper:]' '[:lower:]'`
    caps='-i'
    shopt -s nocasematch
  fi
#echo "search = $search"
  IFS='|'; read -ra words <<< "$search"
  let wc=${#words[@]}
#echo "wc = $wc"
fi
while read -r; do
    line="$REPLY"
    skip=
#echo "ol_line = '$line'" >&2
    if [ -n "$search" ]; then
      skip='true'
      if [ "$search_type" = 'excl' ]; then
        if [[ ${line,,} != *"$search"* ]]; then
#echo "excl = $line" > /dev/stderr  
           skip=
        fi
      else
       if [ $wc -gt 1 ];then
        let wcnt=0
        ll="$line"
        for i in "${words[@]}"; do
           let c=`echo $line | grep "$i" -c $caps`
           if [ $c -ne 0 ];then
#echo "word $i"
             let "wcnt++"
           fi
        done
        if [ -n "$and" ]; then
          if [ $wcnt -eq $wc ]; then
#echo "line = $line"
#exit
            skip=
          fi
        else
          if [ $wcnt -ge 1 ]; then
#echo "line = $line"
#exit
            skip=
          fi
        fi
       else
        if [[ $line = *"$search"* ]]; then
#         if [[ $line =~ $search ]]; then
#echo "inc = $line, search = $search" > /dev/stderr  
           skip=
          fi
        fi
      fi
    elif [ "$search_type" != 'pub' ]; then
      if [[ $line = *public/* ]] || [[ $line = */fontello/* ]]; then
        skip='true'
#echo "skip = '$line'"    
      fi
    fi
#echo "skip = '$skip'"    
    if [ -z "$skip" ]; then
#echo "line = $line" > /dev/stderr  
      line_out=
      stat=${line:0:3}      
      contains "${inc[@]}"
      if [ $? -eq 1 ]; then
        get_line "$by_date" 
      fi   
      if [ -n "$line_out" ]; then
        out="$out$line_out"
      fi
    fi  
#    let "idx++"
#    if [ $idx -gt 1 ]; then
#      exit
#    fi  
  done <<< "$list"

  idx=0
  while read -r line; do
    if [ -n "$line" ]; then
      read -a item <<< $line
#echo "idx = $idx, item = '$item', item[0] = '${item[0]}', item[1] = '${item[1]}', item[2] = '${item[2]}'"
      let "idx++"
      if [ -z "$quiet" ]; then
        echo "$idx. $line"
      fi
      if [ -n "$by_date" ];then
        sidx=2
      else
        sidx=0
      fi
      stats[$idx]="${item[0]}"
      lines[$idx]="${item[$sidx]}"
    fi

  done <<< `echo "$out" | sort`
  
  return $idx
  }
  
function is_numeric ()
  {
  re='^[0-9]+$'
  if ! [[ $1 =~ $re ]] ; then
    return 0
  fi
  return 1
}

  
