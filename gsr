#!/bin/bash
if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
  echo "$(basename $0) - Print remote spec or clone repo"
  exit
fi


function usage
  {
  echo 'Usage: gsr [ OPTIONS ]'
  echo
  echo 'Print remote specification or clone to directory above this one.'
  echo
  echo 'Options:'
  echo '  -h Prints this usage information.'
  echo
  echo "  -o Alternate origin name.  Default is 'origin', of course."
  echo
  echo '  -a Add Remote spec'
  echo
  echo '  -r Remove remote'
  echo
  echo '  -c Directory name to clone to'
  echo
  echo "  -y Don't prompt, just clone it"
  echo
  echo " Use use the -c option to name a git clone destination directory.  Without"
  echo " the -c option script will only print the remote spec (default)."
exit 1
  }

origin='origin'
while getopts "o:c:hra:" opt; do
  case $opt in
    o)
      origin="$OPTARG"
      origin_msg=" to '$origin'"
      ;;
    c)
      clone_dir="$OPTARG"
      ;;
    a)
      remote="$OPTARG"
      ;;
    r)
      remove='true'
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

is_repo=`ls -a .git 2>/dev/null`

if [ -z "$is_repo" ]; then
  echo 'This script can only be run in a git workspace root directory'
  exit
fi

if [ -n "$remote" ]; then
echo "  git remote add $origin $remote"
  git remote add "$origin" "$remote"
  exit
fi

rem=`git remote show $origin | grep Fetch | awk '{print $3 }'`

if [ -n "$clone_dir" ]; then
  cd ..
  dir=`pwd`
  if [ -d $clone_dir ]; then
     echo "$dir/$clone_dir already exists"
     exit
  fi
  if [ -z "$no_prompt" ];then
    echo -n "Are you sure you want to clone '$rem' to $dir/$clone_dir - (yN) "
    read -n 1 ans
    if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
      echo
      exit
    fi  
      echo
  fi
  git clone $rem $clone_dir
elif [ -n "$remove" ]; then
  if [ -z "$no_prompt" ];then
    echo -n "Are you sure you want to remove remote '$rem' - (yN) "
    read -n 1 ans
    if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
      echo
      exit
    fi  
      echo
  fi
  git remote remove "$origin"
else
  echo $rem
fi  

