#!/bin/bash

if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
  echo "$(basename $0) - List all tags to remove"
  exit
fi


function has_remote_tag ()
  {
  if [ $# -eq 2 ]; then
    ck_tag='true'
  fi
  remote_tags=`git ls-remote --tags origin | awk '{print $2}' | awk -F/ '{print $3}'`

  if [ -n "$remote_tags" ];then
    let idx=0
    while read -r tg; do
      if [[ $tg != *"^{}"* ]]; then
#    echo "remote tag = $tg and param = $1"
        if [ "$tg" = "$1" ]; then
          if [ -n "$ck_tag" ]; then
            echo "$1 tag does exist on remote"
            echo
          fi
          has_remote='true'
          return
        fi  
      fi
    done <<< "$remote_tags"
    if [ -n "$ck_tag" ]; then
      echo "$1 tag does NOT exist on remote"
      echo
    fi
  fi
  }
  
function is_numeric ()
  {
  re='^[0-9]+$'
  if ! [[ $1 =~ $re ]] ; then
    return 0
  fi
  return 1
}

function usage
  {
  echo "Usage: $(basename $0) [ options ] [ tag index | tag name ]"
  echo
  echo 'Prints numbered list of currently defined tags and allows selection of tag to remove.'
  echo
  echo 'Options:'
  echo '  -h Prints this usage information.'
  echo '  -a Allow alpha chracters in tag name.  Default converts command line tag names'
  echo '     to numeric via regex. e.g. 1.0.1'
  echo "  -y Don't prompt, just do it."
  echo "  -c Create tag. You must provide a new tag name."
  echo "  -P Don't push tag."
  echo "  -r remove or push remote tag as well.  Push if creating a new tag, otherwise remove remote tag."
  echo "  -k check if tag exists on remote and exit."
  echo "  -R Append '_RCx to tag name, where 'x' is numerical parameter that folows option."
  echo "  -o remote name to delete from or push to, defaults to 'origin'"
  echo '  -n Dry run (do nothing).  Enables verbose.'
  echo '  -v Verbose'
  echo
  echo "At the select prompt you can list multiple indexes for those tags you want to remove.  Separate each with a space."
  echo
exit 1
  }

do_remote=  
create=
remote_msg=
repo='origin'
action='remove'
has_remote=
while getopts "rhycPavnR:o:k:" opt; do
  case $opt in
    c)
      create='true'
      action='create'
      ;;
    p)
      no_push='false'
      ;;
    r)
      do_remote='true'
      remote_msg=' both remote and'
      ;;
    a)
      alpha='true'
      ;;
    R)
      rc="$OPTARG"
      ;;
    o)
      repo="$OPTARG"
      ;;
    y)
      no_prompt='true'
      ;;
    k)
      has_remote_tag "$OPTARG" 'ck'
      exit
      ;;
    v)
      verbose='true'
      ;;
    n)
      dry_run='true'
      verbose='true'
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

if [ -n "$1" ]; then
  if [ -z "$alpha" ]; then
    is_numeric $1
    if [ $? -eq 0 ] || [ -n "$create" ]; then
      tag_provided='true'
    fi
  else
    tag_provided='true'
  fi
fi

idx=0
if [ -z "$tag_provided" ]; then  
  list=`git tag`
  while read -r tr; do
    let idx=$[idx + 1]
    tags[$idx]=$tr
    echo "${idx}. $tr"
  done <<< "$list"
fi

if [ -n "$1" ]; then 
  if [ -z "$tag_provided" ]; then
    tag=${tags[$1]}
  else
    tag="$1"
  fi
fi

let idx_len=1
if [ -z "$tag" ]; then
  echo
  echo -n "Enter index of tag(s) to delete (CR to exit): "
  read line
  if [ -z "$line" ]; then
    exit
  fi
  idxs=($line)
  let idx_len=${#idxs[@]}
  if [ $idx_len -eq 1 ]; then
    tag=${tags[${idxs[0]}]}
    if [ -z "$tag" ]; then
      echo "Invalid selection"
      exit
    fi
  else
    for i in "${idxs[@]}"; do
      tg="${tags[$i]}"
      if [ -n "$tg" ]; then
        itms+=("$tg")
      fi  
    done
  fi
  create=
fi

if [ -n "$do_remote" ]; then
  if [ -n "$create" ];then
    remote_msg=" and push to $repo"
  else
    remote_msg=" and delete from $repo"
  fi
fi

if [ $idx_len -eq 1 ]; then
  if [ -z "$tag" ]; then
    echo 'You must supply a tag name or index on command line.'
    usage
  fi

  if [ -n "$create" ];then
    if  [[ "$tag" = *"RC"* ]]; then
      srch='_RC'
      num=${tag#*$srch}
      tag_name="Release Candidate $num"
    elif [ -n "$rc" ]; then
      tag="${tag}_RC$rc"
      tag_name="Release Candidate $rc"
    else
      tag_name="Version $tag"
    fi
  fi  
  
  if [ -z "$no_prompt" ];then
    echo -n "Are you sure you want to $action local tag '$tag'$remote_msg - (yN) "
    read -n 1 ans
    if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
      echo
      exit
    fi  
      echo
  fi

  has_remote_tag $tag
  if [ -n "$create" ];then
    if [ -n "$verbose" ]; then
      echo "git tag -a '$tag' -m '$tag_name'"
    fi  
    if [ -z "$dry_run" ]; then
      git tag -a $tag -m "'$tag_name'"
    fi  
    if [ -z "$no_push" ]; then
      if [ -z "$has_remote" ]; then
        if [ -n "$verbose" ]; then
          echo "git push $repo $tag"
        fi  
        if [ -z "$dry_run" ]; then
          git push $repo $tag
        fi  
      else
        echo "Remote tag $tag already exists on $repo, so not pushed."
        echo "You must remove newly created local tag.  e.g. $(basename $0) -r $tag"
      fi
    fi  
  else
    # delete tag is the default.  No -c option.
    if [ -z "$dry_run" ]; then
      git tag -d "$tag"
    fi  
    if [ -n "$verbose" ]; then
      echo "git tag -d '$tag'"
    fi  
    if [ -n "$do_remote" ] && [ -z "$no_push" ]; then
      if [ -n "$has_remote" ]; then
        if [ -n "$verbose" ]; then
          echo "git push --delete $repo $tag"
        fi  
        if [ -z "$dry_run" ]; then
          git push --delete $repo $tag
        fi   
      else
        echo "Remote tag $tag does not exist on $repo, so no remote delete"
      fi
    fi
  fi
else
  # tag(s) selected from list.  Delete only.
  if [ -z "$no_prompt" ];then
    echo "Are you sure you want to delete local tag{s)$remote_msg"
    for tg in "${itms[@]}"; do
       echo -e "\t$tg"
    done
    echo -n "(yN) "
    read -n 1 ans
    if [ "$ans" != 'y' ] && [ "$ans" != 'Y' ];then
      echo
      exit
    fi  
  fi
  echo
  if [ -n "$dry_run" ]; then
    echo "Deleted:"
  fi  
  for tag in "${itms[@]}"; do
    if [ -n "$do_remote" ]; then
      has_remote_tag $tag
      if [ -n "$has_remote" ]; then
        if [ -n "$verbose" ]; then
          echo "git push --delete $repo $tag"
        fi  
        if [ -z "$dry_run" ]; then
          git push --delete $repo $tag
        fi  
      else
        echo "Remote tag $tag does not exist on $repo, so no remote delete"
      fi
    else  
      if [ -n "$verbose" ]; then
        echo "git tag --delete '$tag'"
      fi  
      if [ -z "$dry_run" ]; then
        git tag --delete "$tag"
      fi  
    fi
    if [ -z "$verbose" ] && [ -n "$dry_run" ]; then
      echo -e "\t$tag"
    fi  
  done
fi
echo
exit
