#!/bin/bash
  if [ "$1" = '?' ] || [ "$1" = '-?' ]; then
    echo "$(basename $0) - Select file to diff"
    exit
  fi
  
if [ $# -eq 0 ]; then
  echo 'git diff $file'
  echo 'You must include path to file'
  exit
fi
git diff $1
